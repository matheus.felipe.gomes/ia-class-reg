**Escola Politécnica da Universidade de São Paulo**

**Disciplina**: PCS3438 - Inteligência Artificial

**Nome**: Matheus Felipe Gomes

**NUSP**: 8993198

**Data**: 23 de Novembro de 2020

**Professor**: Dr. Eduardo R. Hruschka

**Título**: Lista de Exercícios de Classificação e Regressão

---

A estrutura do diretório de trabalho:

```
.
├── README.md
├── data
│   ├── class01.csv
│   ├── class02.csv
│   ├── reg01.csv
│   └── reg02.csv
├── doc
│   └── lex.pdf
├── lex_class_reg.ipynb
└── lex_class_reg.md

2 directories, 8 files
```

A seguir, o conteúdo do jupyter notebook `lex_class_reg.ipynb`.

```python
import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import Lasso
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
```

# Questão 1


```python
class01_path = "./data/class01.csv"
class01_df = pd.read_csv(class01_path)
class01_df_train = class01_df.iloc[:350, :]
class01_df_validation = class01_df.iloc[350:, :]
class01_np_train_X = class01_df_train.iloc[:, :-1].to_numpy()
class01_np_train_y = class01_df_train.iloc[:, -1].to_numpy()
class01_np_validation_X = class01_df_validation.iloc[:, :-1].to_numpy()
class01_np_validation_y = class01_df_validation.iloc[:, -1].to_numpy()
gnb_clf = GaussianNB()
gnb_clf.fit(class01_np_train_X, class01_np_train_y)
validation_pred = gnb_clf.predict(class01_np_validation_X)
train_pred = gnb_clf.predict(class01_np_train_X)
validation_acc = accuracy_score(class01_np_validation_y, validation_pred)
train_acc = accuracy_score(class01_np_train_y, train_pred)
print(f"valor da acurácia para a base de treino: {train_acc};")
print(f"valor da acurácia para a base de validação: {validation_acc};")
```

    valor da acurácia para a base de treino: 0.76;
    valor da acurácia para a base de validação: 0.6276923076923077;


# Questão 2


```python
class02_path = "./data/class02.csv"
class02_df = pd.read_csv(class02_path)
class02_len = len(class02_df)
num_folds = 5
fold_len = int((1/num_folds) * class02_len)
validation_acc_list = []
train_acc_list = []

# k-fold cross-validation loop
for i in range(num_folds):
    train_idx = list(np.arange(0, i*fold_len, 1)) + list(np.arange((i+1)*fold_len, class02_len, 1))
    validation_idx = list(np.arange(i*fold_len, (i+1)*fold_len, 1))
    train_df = class02_df.iloc[train_idx, :]
    train_np_X = train_df.iloc[:, :-1].to_numpy()
    train_np_y = train_df.iloc[:, -1].to_numpy()
    validation_df = class02_df.iloc[validation_idx, :]
    validation_np_X = validation_df.iloc[:, :-1].to_numpy()
    validation_np_y = validation_df.iloc[:, -1].to_numpy()
    knn_clf = KNeighborsClassifier(n_neighbors=10)
    knn_clf.fit(train_np_X, train_np_y)
    train_pred = knn_clf.predict(train_np_X)
    train_acc = accuracy_score(train_np_y, train_pred)
    train_acc_list.append(train_acc)
    validation_pred = knn_clf.predict(validation_np_X)
    validation_acc = accuracy_score(validation_np_y, validation_pred)
    validation_acc_list.append(validation_acc)

mean_train_acc = np.mean(train_acc_list)
mean_validation_acc = np.mean(validation_acc_list)
print(f"valor médio da acurácia para a base de treino: {mean_train_acc};")
print(f"valor médio da acurácia para a base de validação: {mean_validation_acc};")
```

    valor médio da acurácia para a base de treino: 0.8661666666666668;
    valor médio da acurácia para a base de validação: 0.8386666666666667;


# Questão 3


```python
reg01_path = "./data/reg01.csv"
reg01_df = pd.read_csv(reg01_path)
reg01_df_len = len(reg01_df)
validation_y_true_list = []
validation_y_pred_list = []
train_y_true_list = []
train_y_pred_list = []

# Leave-One-Out loop
for i in range(reg01_df_len):
    train_idx = list(np.arange(0, i, 1)) + list(np.arange(i+1, reg01_df_len, 1))
    train_df = reg01_df.iloc[train_idx, :]
    train_np_X = train_df.iloc[:, :-1].to_numpy()
    train_np_y = train_df.iloc[:, -1].to_numpy()
    validation_df = reg01_df.iloc[i, :]
    validation_np_X = validation_df.iloc[:-1].to_numpy()
    validation_np_y = validation_df.iloc[-1]
    lasso_clf = Lasso()
    lasso_clf.fit(train_np_X, train_np_y)
    validation_y_pred = lasso_clf.predict(validation_np_X.reshape(1,-1))
    validation_y_true_list.append(np.array([validation_np_y]))
    validation_y_pred_list.append(validation_y_pred)
    train_y_pred = lasso_clf.predict(train_np_X)
    train_y_true_list.append(train_np_y)
    train_y_pred_list.append(train_y_pred)

validation_rmse = 0
train_rmse = 0
for i in range(reg01_df_len):
    validation_rmse += mean_squared_error(validation_y_true_list[i], validation_y_pred_list[i], squared=False)
    train_rmse += mean_squared_error(train_y_true_list[i], train_y_pred_list[i], squared=False)

mean_validation_rmse = validation_rmse / reg01_df_len
mean_train_rmse = train_rmse / reg01_df_len

print(f"valor médio do RMSE para a base de treino: {mean_train_rmse};")
print(f"valor médio do RMSE para a base de validação: {mean_validation_rmse};")
```

    valor médio do RMSE para a base de treino: 19.220259837710355;
    valor médio do RMSE para a base de validação: 15.465218791702423;


# Questão 4


```python
reg02_path = "./data/reg02.csv"
reg02_df = pd.read_csv(reg02_path)
reg02_df_len = len(reg02_df)
num_folds = 5
fold_len = int((1/num_folds) * reg02_df_len)
validation_mae_list = []
train_mae_list = []

# k-fold cross-validation loop
for i in range(num_folds):
    train_idx = list(np.arange(0, i*fold_len, 1)) + list(np.arange((i+1)*fold_len, reg02_df_len, 1))
    validation_idx = list(np.arange(i*fold_len, (i+1)*fold_len, 1))
    train_df = reg02_df.iloc[train_idx, :]
    train_np_X = train_df.iloc[:, :-1].to_numpy()
    train_np_y = train_df.iloc[:, -1].to_numpy()
    validation_df = reg02_df.iloc[validation_idx, :]
    validation_np_X = validation_df.iloc[:, :-1].to_numpy()
    validation_np_y = validation_df.iloc[:, -1].to_numpy()
    dt_clf = DecisionTreeRegressor()
    dt_clf.fit(train_np_X, train_np_y)
    train_pred = dt_clf.predict(train_np_X)
    train_mae = mean_absolute_error(train_np_y, train_pred)
    train_mae_list.append(train_mae)
    validation_pred = dt_clf.predict(validation_np_X)
    validation_mae = mean_absolute_error(validation_np_y, validation_pred)
    validation_mae_list.append(validation_mae)

mean_train_mae = np.mean(train_mae_list)
mean_validation_mae = np.mean(validation_mae_list)
print(f"valor médio do MAE para a base de treino: {mean_train_mae};")
print(f"valor médio do MAE para a base de validação: {mean_validation_mae};")
```

    valor médio do MAE para a base de treino: 0.0;
    valor médio do MAE para a base de validação: 44.46728454595148;

